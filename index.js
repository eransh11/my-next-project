const outerMostConatiner=document.querySelector(".outer_most_conatiner")
const closeModalButton=document.querySelector(".close_modal_cross_btn")
const openModalButton=document.querySelectorAll("#open_Modal_Btn")
const formPurchaseButton=document.querySelector("#formPurchaseButton")
const formCont=document.querySelector(".purchase_form")

const freeCard=document.querySelector("#freeCard")
const proCard=document.querySelector("#proCard")
const enterpriseCard=document.querySelector("#enterpriseCard")

const range = document.querySelector(".range");
const bubble = document.querySelector(".bubble");
range.value=1
freeCard.classList.add("border-primary")

const handleCloseModal=()=>{

    document.querySelector(".modal_Outer_Container").style.display="none"
    outerMostConatiner.style.overflow="auto"
}
const openModal=()=>{
    window.scrollTo({top: 0, behavior: 'smooth'});
    document.querySelector(".modal_Outer_Container").style.display="block"
    outerMostConatiner.style.overflow="hidden"
}

const handleFormSubmit=async(e)=>{
    e.preventDefault()
    const name=document.querySelector("#floatingName").value
    const email=document.querySelector("#floatingemailAddress").value
    const message=document.querySelector("#floatingOrderComments").value
    console.log(name,
        email,
        message)
  try {
    let formData = new FormData()
    formData.append('firstname', name)
    formData.append('email', email)
    formData.append('message',message)
    formPurchaseButton.disabled = true
    const res = await fetch(`https://forms.maakeetoo.com/formapi/463`, {
        body: formData,
        headers: {
            // 'Content-Type': 'application/x-www-form-urlencoded',
            'Code':'Q3NQSI1I7J5E947MVJWEI5AAV',
          },
        method: 'POST',
        mode: 'no-cors'
    });
    console.log("res",res)
    formPurchaseButton.disabled = false
    alert('Form Successfuly Submitted')
    document.querySelector("#floatingName").value=""
    document.querySelector("#floatingemailAddress").value=""
    document.querySelector("#floatingOrderComments").value=""
  } catch (error) {
    alert(error)
    formPurchaseButton.disabled = false
  }


}

closeModalButton.addEventListener('click',handleCloseModal)
openModalButton.forEach((btn)=>btn.addEventListener('click',openModal))
formCont.addEventListener("submit",handleFormSubmit)

function setBubble(range, bubble) {
  const val = range.value;

if(val>0&val<=10){
  freeCard.classList.add("border-primary")
  freeCard.style.borderWidth="2px"
  proCard.classList.remove("border-primary")
  proCard.style.borderWidth="0.5px"
enterpriseCard.classList.remove("border-primary")
enterpriseCard.style.borderWidth="0.5px"
}
if(val>10&val<=20){
  freeCard.classList.remove("border-primary")
  freeCard.style.borderWidth="0.5px"
  proCard.classList.add("border-primary")
  proCard.style.borderWidth="2px"
enterpriseCard.classList.remove("border-primary")
enterpriseCard.style.borderWidth="0.5px"
}
if(val>20){
  freeCard.classList.remove("border-primary")
  freeCard.style.borderWidth="0.5px"
  proCard.classList.remove("border-primary")
  proCard.style.borderWidth="0.5px"
enterpriseCard.classList.add("border-primary")
enterpriseCard.style.borderWidth="2px"
}

  const min = range.min ? range.min : 0;
  const max = range.max ? range.max : 100;
  const newVal = Number(((val - min) * 100) / (max - min));
  bubble.innerHTML = val;

  // Sorta magic numbers based on size of the native UI thumb
  bubble.style.left = `calc(${newVal}% + (${8 - newVal * 0.20}px))`;
}

range.addEventListener("input", (e) => {

  setBubble(range, bubble);
});
setBubble(range, bubble);
