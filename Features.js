const userList = document.getElementById('userList');
const loadingIndicator = document.getElementById('loadingIndicator');
let page = 1;
let isLoading = false;
let finalData=[]
// Function to fetch and display user data
async function fetchUsers() {
  isLoading = true;
  loadingIndicator.style.display = 'block';
  
 setTimeout(async()=>{
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/users?_page=${page}&_limit=10`);
        const users = await response.json();
     
        if(users.length===0&&page!==5){
            finalData.forEach(user => {
                const userCard = document.createElement('div');
                userCard.className = 'user-card';
              //   userCard.textContent = `Name: ${user.name}, Email: ${user.email}`;
                userCard.innerHTML = `
                <h1>Name : <span>${user.name}</span></h1> 
                <h1>Email : <span>${user.email}</span></h1>
                `;
                userList.appendChild(userCard);
              });
              isLoading = false;
        loadingIndicator.style.display = 'none';
        page++;
            return 
        }
    if(users.length>0){
        finalData.push(...users)
        users.forEach(user => {
            const userCard = document.createElement('div');
            userCard.className = 'user-card';
          //   userCard.textContent = `Name: ${user.name}, Email: ${user.email}`;
            userCard.innerHTML = `
            <h1>Name : <span>${user.name}</span></h1> 
            <h1>Email : <span>${user.email}</span></h1>
            `;
            userList.appendChild(userCard);
          });
    }
    
    
    
        isLoading = false;
        loadingIndicator.style.display = 'none';
        page++;
      } catch (error) {
        console.error('Error fetching data:', error);
        isLoading = false;
        loadingIndicator.style.display = 'none';
      }
 },1500)
}

// Function to check if the user has scrolled to the bottom
function isAtBottom() {
  return window.innerHeight + window.scrollY >= document.body.offsetHeight;
}

// Event listener for scrolling
window.addEventListener('scroll', () => {
  if (isAtBottom() && !isLoading) {
    if(page!==5){
        fetchUsers();
    }
    else{
        document.querySelector(".reachTheEndText").style.display="block"
    }
   
  }
});

// Initial data fetch
fetchUsers();